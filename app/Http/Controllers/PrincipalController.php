<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Medicoes;

class PrincipalController extends Controller
{
    public function principal(){
        return view('principal');
    }
    public function principalBaterias(){
        return view('principalBaterias');
    }
    public function teoria(){
        return view('teoria');
    }
    public function procedimento(){
        return view('procedimento');
    }
    public function medicoes(){
        $medicoes = Medicoes::all();
        return view('medicoes',compact('medicoes'));
    }
    public function conclusoes(){
        return view('conclusoes');
    }
    public function pagina1(){
        return view('pagina1');
    }
    public function pagina2(){
        return view('pagina2');
    }
    
}
