<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PrincipalController;
use Faker\Guesser\Name;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/principalBaterias', [PrincipalController::class, 'principalBaterias'])-> Name('/');
Route::get('/teoria', [PrincipalController::class, 'teoria'])-> Name('/');
Route::get('/procedimento', [PrincipalController::class, 'procedimento'])-> Name('/');
Route::get('/medicoes', [PrincipalController::class, 'medicoes'])-> Name('/');
Route::get('/conclusoes', [PrincipalController::class, 'conclusoes'])-> Name('/');
Route::get('/principal', [PrincipalController::class, 'principal']);



Route::get('/pagina1', [PrincipalController::class, 'pagina1']);

Route::get('/pagina2', [PrincipalController::class, 'pagina2']);
